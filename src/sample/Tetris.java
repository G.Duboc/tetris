package sample;

import sample.blocs.*;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Tetris {

    private int score;
    private int line;
    private int[][] fieldArray;
    private LocalTime timerStart;
    private int timer;

    Tetris(int[][] fieldArray) {
        this.fieldArray = fieldArray;
        timerStart = LocalTime.now();
        score = 0;
        line = 0;
    }

    public int getScore() {
        return score;
    }

    public void createBloc(Bloc bloc) {
        if (!bloc.isComplicated()) {
            for (int i = 0; i < bloc.getSimpleForm().length; i++) {
                fieldArray[fieldArray.length / 2 + i][0] = bloc.getSimpleForm()[i];
            }
        } else {
            for (int i = 0; i < bloc.getComplicatedForm().length; i++) {
                for (int j = 0; j < bloc.getComplicatedForm()[i].length; j++) {
                    fieldArray[fieldArray.length / 2 + i][j] = bloc.getComplicatedForm()[i][j];
                }
            }
        }
    }

    public int getLine() {
        return line;
    }

    public Bloc getRandomBloc() {
        int randomNumber = (int) (Math.random() * 4) + 1;
        switch (randomNumber) {
            case 1:
                return new RightLBloc();
            case 2:
                return new LineBloc();
            case 3:
                return new SquareBloc();
            case 4:
                return new LeftLBloc();
            default:
                return new SimpleBloc();
        }
    }

    public boolean isMovingBlockStuck(int[][] fieldArray, int fieldHeight) {

        for (int i = 0; i < fieldArray.length; i++) {
            if (isBlocMoving(fieldArray[i][fieldHeight - 1])) {
                return true;
            }
        }
        for (int i = 0; i < fieldArray.length; i++) {
            for (int j = 0; j < fieldArray[i].length - 1; j++) {
                if (isBlocMoving(fieldArray[i][j]) && (fieldArray[i][j + 1] > 100)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isBlocMovable(int[][] fieldArray, int direction) {
        for (int i = 0; i < fieldArray.length; i++) {
            for (int j = 0; j < fieldArray[0].length; j++) {
                if (isBlocMoving(fieldArray[i][j]) && direction < 0 && i == 0) {
                    return false;
                } else if (isBlocMoving(fieldArray[i][j]) && direction > 0 && i == fieldArray.length - 1) {
                    return false;
                } else if (isBlocMoving(fieldArray[i][j]) && (fieldArray[i + direction][j] > 100)) {
                    return false;
                }
            }
        }
        return true;
    }

    public int[][] moveBlockLeft(int[][] fieldArray) {
        if (isBlocMovable(fieldArray, -1)) {
            for (int i = 0; i < fieldArray.length; i++) {
                for (int j = 0; j < fieldArray[i].length; j++) {
                    if (isBlocMoving(fieldArray[i][j])) {
                        if (fieldArray[i - 1][j] == 0) {
                            fieldArray[i - 1][j] = fieldArray[i][j];
                            fieldArray[i][j] = 0;
                        }
                    }
                }
            }
        }
        return fieldArray;
    }

    public int[][] moveBlockRight(int[][] fieldArray) {
        if (isBlocMovable(fieldArray, 1)) {
            for (int i = fieldArray.length - 1; i >= 0; i--) {
                for (int j = 0; j < fieldArray[i].length; j++) {
                    if (isBlocMoving(fieldArray[i][j])) {
                        if (fieldArray[i + 1][j] == 0) {
                            fieldArray[i + 1][j] = fieldArray[i][j];
                            fieldArray[i][j] = 0;
                        }
                    }
                }
            }
        }
        return fieldArray;
    }

    public int[][] moveBlockDown(int[][] fieldArray) {
        for (int i = fieldArray.length - 1; i >= 0; i--) {
            for (int j = fieldArray[i].length - 1; j >= 0; j--) {
                if (isBlocMoving(fieldArray[i][j])) {
                    if (j == fieldArray[i].length - 1) {
                        fieldArray[i][j] = 2;
                    } else if (fieldArray[i][j + 1] == 0)
                        fieldArray[i][j + 1] = fieldArray[i][j];
                    fieldArray[i][j] = 0;
                }
            }
        }
        return fieldArray;
    }

    public int[][] fixBlocks(int[][] fieldArray) {
        for (int i = fieldArray.length - 1; i >= 0; i--) {
            for (int j = fieldArray[i].length - 1; j >= 0; j--) {
                if (isBlocMoving(fieldArray[i][j])) {
                    fieldArray[i][j] += 100;
                }
            }
        }
        return fieldArray;
    }

    public boolean checkGameOver(int[][] fieldArray) {
        for (int i = 0; i < fieldArray.length; i++) {
            if (fieldArray[i][0] > 0) {
                return true;
            }
        }
        return false;
    }

    public boolean isBlocMoving(int blocNumber) {
        return (blocNumber < 100 && blocNumber > 0);
    }

    public int[][] isLineToBeDeleted(int[][] fieldArray) {
        int counter = 0;
        for (int i = 0; i < fieldArray[0].length; i++) {
            for (int j = 0; j < fieldArray.length; j++) {
                if (fieldArray[j][i] > 100) {
                    counter++;
                }
            }

            if (counter == fieldArray.length) {
                deleteLine(fieldArray, i);
            }
            counter = 0;
        }

        return fieldArray;
    }

    public int[][] deleteLine(int[][] fieldArray, int maxLine) {
        System.out.println("Line " + maxLine + " deleted !");
        line++;
        score += (100 + (int) ChronoUnit.SECONDS.between(timerStart, LocalTime.now()));
        for (int i = maxLine; i > 0; i--) {
            for (int j = 0; j < fieldArray.length; j++) {
                System.out.println("J : " + j + " - I : " + i);
                fieldArray[j][i] = fieldArray[j][i - 1];
            }
        }
        return fieldArray;
    }

    public int updateTimer() {
        timer = (int) ChronoUnit.SECONDS.between(timerStart, LocalTime.now());
        return timer;
    }
}
