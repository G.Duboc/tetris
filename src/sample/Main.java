package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent root = loader.load();
        Controller c = loader.getController();
        Scene scene = new Scene(root, 800, 800);
        primaryStage.getIcons().add(new Image("Logo.jpg"));
        String css = this.getClass().getResource("stylesheet/StyleSheet.css").toExternalForm();
        scene.getStylesheets().add(css);

        primaryStage.setTitle("TETRIS");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

        InputHandler inputHandler = new InputHandler(c);
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                inputHandler.onKeyPressed(event);
            }
        });

    }

    public static void main(String[] args) {
        launch(args);
    }
}
