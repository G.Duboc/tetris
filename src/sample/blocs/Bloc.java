package sample.blocs;

public abstract class Bloc {
    private int[] simpleForm;
    private int[][] complicatedForm;
    private boolean isComplicated;

    public Bloc() {
    }


    public int[] getSimpleForm() {
        return simpleForm;
    }

    public int[][] getComplicatedForm() {
        return complicatedForm;
    }

    public void setSimpleForm(int[] simpleForm) {
        this.simpleForm = simpleForm;
    }

    public void setComplicatedForm(int[][] complicatedForm) {
        this.complicatedForm = complicatedForm;
    }

    public void setComplicated(boolean complicated) {
        isComplicated = complicated;
    }

    public boolean isComplicated() {
        return isComplicated;
    }

}
