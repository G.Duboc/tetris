package sample.blocs;

public class LineBloc extends Bloc {

    private boolean vertical;
    private int colorCode = 20;

    public LineBloc() {
        this.setSimpleForm(new int[]{colorCode, colorCode, colorCode, colorCode});
        this.setComplicated(false);
        this.vertical = false;
    }

    public boolean isVertical() {
        return vertical;
    }

    //Main mehtod for turning the bloc
    public int[][] turn(int[][] inputArray) {
        int[] center = findCenter(inputArray);

        if (isTurnable(center, inputArray)) {
            boolean oneDirection = true;
            int counter = 1;

            if (this.isVertical()) {
                this.vertical = false;
                for (int i = 0; i < 4; i++) {
                    inputArray[center[0]][center[1] + i] = 0;

                    if (center[0] + i < inputArray.length) {
                        if (inputArray[center[0] + i][center[1]] == 0 && oneDirection) {
                            inputArray[center[0] + i][center[1]] = colorCode;
                        } else {
                            oneDirection = false;
                            inputArray[center[0] - counter][center[1]] = colorCode;
                            counter++;
                        }
                    } else {
                        inputArray[center[0] - counter][center[1]] = colorCode;
                        counter++;
                    }
                }
            } else {
                this.vertical = true;
                for (int i = 0; i < 4; i++) {
                    inputArray[center[0] + i][center[1]] = 0;
                    inputArray[center[0]][center[1] + i] = colorCode;
                }
            }
        } else {
            System.out.println("Item not turnable.");
        }


        return inputArray;
    }

    //Find the pivoting spot of the LineBloc
    public int[] findCenter(int[][] inputArray) {
        int[] center = new int[2];
        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray[0].length; j++) {
                if (inputArray[i][j] > 0 && inputArray[i][j] < 100) {
                    center[0] = i;
                    center[1] = j;
                    return center;
                }
            }
        }
        return center;
    }

    //Line is turnable if there is some free space below or left/right from the pivoting spot
    public boolean isTurnable(int[] center, int[][] inputArray) {
        int placeLeft = 0;
        int placeRight = 0;
        int placeDown = 0;

        while (center[0] + placeLeft > 0) {
            if ((inputArray[center[0] + (placeLeft - 1)][center[1]] == 0) || (inputArray[center[0] + (placeLeft - 1)][center[1]] == colorCode)) {
                placeLeft--;
            } else {
                break;
            }
        }

        while (center[0] + placeRight < inputArray.length - 1) {
            if ((inputArray[center[0] + placeRight + 1][center[1]] == 0) || (inputArray[center[0] + placeRight + 1][center[1]] == colorCode)) {
                placeRight++;
            } else {
                break;
            }
        }

        while (center[1] + placeDown < inputArray[0].length - 1) {
            if ((inputArray[center[0]][center[1] + placeDown + 1] == 0) || (inputArray[center[0]][center[1] + placeDown + 1] == colorCode)) {
                placeDown++;
            } else {
                break;
            }
        }
        return ((placeDown > 2) && (placeRight - placeLeft > 2));
    }

}
