package sample.blocs;

public class SimpleBloc extends Bloc {

    public SimpleBloc() {
        this.setSimpleForm(new int[]{1});
        this.setComplicated(false);
    }
}
