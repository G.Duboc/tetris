package sample.blocs;

public class RightLBloc extends Bloc {

    private int colorCode = 40;

    public RightLBloc() {
        this.setComplicatedForm(new int[][]{{colorCode, colorCode, colorCode}, {0, 0, colorCode}});
        this.setComplicated(true);
    }

    //Main turn Methode
    public int[][] turn(int[][] inputArray) {

        int[][] tempArray;
        tempArray = isolateForm(inputArray);
        if (isTurnable(tempArray)) {
            inputArray = turnForm(inputArray, tempArray);
        }
        return inputArray;
    }

    //Look for the form in the Array
    public int[][] isolateForm(int[][] intputArray) {
        int[][] result = new int[3][3];
        int MinX = intputArray.length;
        int MinY = intputArray[0].length;

        for (int i = 0; i < intputArray.length; i++) {
            for (int j = 0; j < intputArray[i].length; j++) {
                if (intputArray[i][j] == colorCode) {
                    MinX = Math.min(MinX, i);
                    MinY = Math.min(MinY, j);
                }
            }
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                result[i][j] = intputArray[MinX + i][MinY + j];
            }
        }
        return result;
    }

    //Check if the item is in a right spot to be pivoted
    public boolean isTurnable(int[][] inputArray) {

        int counter = 0;
        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray[i].length; j++) {
                if (inputArray[i][j] == 0) {
                    counter++;
                }
            }
        }
        if (counter == 5) {
            return true;
        } else {
            return false;
        }
    }

    //Pivot the form
    public int[][] turnForm(int[][] intputArray, int[][] testForm) {
        int MinX = intputArray.length;
        int MinY = intputArray[0].length;

        for (int i = 0; i < intputArray.length; i++) {
            for (int j = 0; j < intputArray[i].length; j++) {
                if (intputArray[i][j] == colorCode) {
                    MinX = Math.min(MinX, i);
                    MinY = Math.min(MinY, j);
                }
            }
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                intputArray[MinX + i][MinY + j] = testForm[2 - j][i];
            }
        }

        return intputArray;
    }
}

