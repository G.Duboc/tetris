package sample.blocs;

public class SquareBloc extends Bloc {
    private int colorCode = 10;

    public SquareBloc() {
        this.setComplicatedForm(new int[][]{{colorCode, colorCode}, {colorCode, colorCode}});
        this.setComplicated(true);
    }
}
