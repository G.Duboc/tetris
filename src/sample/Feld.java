package sample;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import sample.blocs.*;

import java.util.ArrayList;


public class Feld {
    int width;
    int height;
    int blocSize;
    Bloc bloc;

    public void setBloc(Bloc bloc) {
        this.bloc = bloc;
    }

    Feld(int width, int height, int blocSize) {
        this.width = width;
        this.height = height;
        this.blocSize = blocSize;
    }

    public void drawFeld(Rectangle someRect) {
        someRect.setHeight((blocSize + 1) * height);
        someRect.setWidth((blocSize + 1) * width);
        System.out.println("Feld drawing");
    }

    public void drawSquares(int[][] inputArray, Pane pane, Rectangle Background) {

        ArrayList<Rectangle> newList = new ArrayList<>();

        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray[i].length; j++) {
                if (inputArray[i][j] == 1) {                            // TEST FORM
                    Rectangle rectangle = new Rectangle(i * (blocSize + 1), j * (blocSize + 1), blocSize, blocSize);
                    rectangle.setFill(Color.DEEPSKYBLUE);
                    newList.add(rectangle);
                } else if (inputArray[i][j] % 100 == 10) {
                    Rectangle rectangle = new Rectangle(i * (blocSize + 1), j * (blocSize + 1), blocSize, blocSize);
                    rectangle.setFill(Color.YELLOWGREEN);
                    newList.add(rectangle);
                } else if (inputArray[i][j] % 100 == 20) {
                    Rectangle rectangle = new Rectangle(i * (blocSize + 1), j * (blocSize + 1), blocSize, blocSize);
                    rectangle.setFill(Color.DEEPSKYBLUE);
                    newList.add(rectangle);
                } else if (inputArray[i][j] % 100 == 30) {
                    Rectangle rectangle = new Rectangle(i * (blocSize + 1), j * (blocSize + 1), blocSize, blocSize);
                    rectangle.setFill(Color.DARKSEAGREEN);
                    newList.add(rectangle);
                } else if (inputArray[i][j] % 100 == 40) {
                    Rectangle rectangle = new Rectangle(i * (blocSize + 1), j * (blocSize + 1), blocSize, blocSize);
                    rectangle.setFill(Color.DARKOLIVEGREEN);
                    newList.add(rectangle);
                }
            }
        }
        pane.getChildren().clear();
        pane.getChildren().add(Background);
        pane.getChildren().addAll(newList);
    }

    public int[][] turnBloc(int[][] inputArray) {

        int[][] resultArray = new int[inputArray.length][inputArray[0].length];

        if (bloc instanceof LineBloc) {
            resultArray = ((LineBloc) this.bloc).turn(inputArray);
        } else if (bloc instanceof LeftLBloc) {
            resultArray = ((LeftLBloc) this.bloc).turn(inputArray);
        } else if (bloc instanceof RightLBloc) {
            resultArray = ((RightLBloc) this.bloc).turn(inputArray);
        } else if (bloc instanceof SquareBloc)
            System.out.println("Square bloc does not move !");

        return resultArray;
    }

}
