package sample;

import javafx.fxml.FXML;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import sample.blocs.*;
import java.io.File;


public class Controller {
    final public int BLOC_SIZE = 35;
    final public int FELD_HEIGHT = 20;
    final public int FELD_WIDTH = 10;

    public int[][] mainArray = new int[FELD_WIDTH][FELD_HEIGHT];
    Feld feld = new Feld(FELD_WIDTH, FELD_HEIGHT, BLOC_SIZE);
    Tetris tetris = new Tetris(mainArray);

    @FXML private ImageView myView;
    @FXML private Image Square;
    @FXML private Image LeftL;
    @FXML private Image RightL;
    @FXML private Image Line;
    @FXML private Rectangle background;
    @FXML private AnchorPane pane;
    @FXML private Button exitButton;
    @FXML private Button newGame;
    @FXML private Label lineLabel;
    @FXML private Label timeLabel;
    @FXML private Label scoreLabel;
    @FXML private Label highscore;
    @FXML private ImageView audio;
    @FXML private ToggleButton musicButton;
    @FXML public BorderPane leftPane;
    private Bloc nextBloc;
    private Timeline recurrentTimer = new Timeline();

    private String clickMusicFile = "Tetris.mp3";
    private Media clickMp3 = new Media(new File(clickMusicFile).toURI().toString());
    private MediaPlayer clickSound = new MediaPlayer(clickMp3);
    private boolean isMusicPlaying = true;


    public Rectangle getBackground() {
        return background;
    }

    public AnchorPane getPane() {
        return pane;
    }

    public int[][] getMainArray() {
        return mainArray;
    }

    public void initialize() {
        initializeLayout();
        callNewBloc();
        feld.drawSquares(mainArray, pane, background);
        musicButton.setGraphic(audio);
        InitializeTimer(1);
    }

    public void InitializeTimer(int temp) {
        recurrentTimer.getKeyFrames().add(new KeyFrame(Duration.seconds(temp), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                runGames();
                setScore();
            }
        }));
        recurrentTimer.setCycleCount(Timeline.INDEFINITE);
        recurrentTimer.play();
    }

    public void initializeLayout () {
        pane.setPadding(new Insets(0, 50, 0, 0));
        musicButton.setFocusTraversable(false);
        newGame.setFocusTraversable(false);
        exitButton.setFocusTraversable(false);
        feld.drawFeld(background);
        nextBloc = tetris.getRandomBloc();
    }

    public void exitButton() {
        System.exit(0);
    }

    public void playMusic() {
        if (isMusicPlaying) {
            clickSound.play();
            isMusicPlaying = false;
        } else {
            clickSound.stop();
            isMusicPlaying = true;
        }
    }

    public void runGames() {
        if (tetris.isMovingBlockStuck(mainArray, FELD_HEIGHT)) {
            if (tetris.checkGameOver(mainArray)) {
                gameOver();
            }
            tetris.fixBlocks(mainArray);
            tetris.isLineToBeDeleted(mainArray);
            callNewBloc();
            setScore();
        } else {
            mainArray = tetris.moveBlockDown(mainArray);
        }
        feld.drawSquares(mainArray, pane, background);
        displayBlocImage();
        tetris.updateTimer();
    }

    public void newGame() {
        mainArray = new int[FELD_WIDTH][FELD_HEIGHT];
        tetris = new Tetris(mainArray);
        nextBloc = tetris.getRandomBloc();
        feld.drawSquares(mainArray, pane, background);
        callNewBloc();
        setScore();
        recurrentTimer.playFromStart();
    }

    public void displayBlocImage() {
        if (nextBloc instanceof LineBloc) {
            myView.setImage(Line);
        } else if (nextBloc instanceof SquareBloc) {
            myView.setImage(Square);
        } else if (nextBloc instanceof RightLBloc) {
            myView.setImage(RightL);
        } else if (nextBloc instanceof LeftLBloc) {
            myView.setImage(LeftL);
        }
    }

    public void callNewBloc() {
        feld.setBloc(nextBloc);
        tetris.createBloc(nextBloc);
        nextBloc = tetris.getRandomBloc();
    }

    public void setScore() {
        int timer = tetris.updateTimer();
        scoreLabel.setText("Score : " + tetris.getScore());
        lineLabel.setText("Lines : " + tetris.getLine());
        timeLabel.setText("Timer : " + timer / 60 + ":" + timer % 60);

        // Update HighScore in case current score is above highscore
        if(tetris.getScore() > Integer.parseInt(highscore.getText().substring(12))){
            highscore.setText("High" + scoreLabel.getText());
        }

    }

    public void gameOver() {
        System.out.println("GAME OVER !");
        recurrentTimer.stop();
    }
}
