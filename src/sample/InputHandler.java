package sample;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class InputHandler {

    private Controller controller;

    public InputHandler(Controller c) {
        this.controller = c;
    }

    public void onKeyPressed(KeyEvent event) {

        if (event.getCode() == KeyCode.RIGHT) {
            controller.tetris.moveBlockRight(controller.mainArray);
            controller.feld.drawSquares(controller.getMainArray(), controller.getPane(), controller.getBackground());
        } else if (event.getCode() == KeyCode.LEFT) {
            controller.tetris.moveBlockLeft(controller.mainArray);
            controller.feld.drawSquares(controller.getMainArray(), controller.getPane(), controller.getBackground());
        } else if (event.getCode() == KeyCode.UP) {
            controller.feld.turnBloc(controller.mainArray);
            controller.feld.drawSquares(controller.getMainArray(), controller.getPane(), controller.getBackground());
        } else if (event.getCode() == KeyCode.DOWN) {
            controller.runGames();
        }
    }
}
